import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import ToolBar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'

class Header extends React.Component {
    constructor(props) {
    super(props)
    }
    render() {
    return <div>
        <AppBar possition="static">
            <ToolBar>

            <Typography variant="headline" color="inherit">
                Database exercises
            </Typography>
           
            </ToolBar>
        </AppBar>
         <br/>
         <br/>
         

    </div>
    }

}

export default Header;