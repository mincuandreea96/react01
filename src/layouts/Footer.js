import React, { Fragment } from 'react'
import { Tabs, Paper, Tab } from '@material-ui/core'


class Footer extends React.Component {
   
    constructor(props) {
        super(props)
    }
    render() {
       
            return <div>
              <Paper  >
                  <Tabs value={0} indicatorColor="primary"  textColor="primary" centered>
                      <Tab label="All"></Tab>
                      {this.props.muscles.map(group=>
                      <Tab label={group}></Tab>
                      
                      )}
                  </Tabs>
              </Paper>
        
        
            </div>
        
    }

}

export default Footer;