import React, { Fragment } from 'react'
import { Grid, Paper, Typography,Tab } from '@material-ui/core'


const styles = {
    Paper: { padding: 20, marginTop: 10, marginBottom: 10 }
}

class Exercises extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return <Grid container sm={12}>
            <Grid item sm={6}>
                <Paper style={styles.Paper}>
                {this.props.exercises.map(group=>
                     <Typography >{group.muscles}</Typography>
                      
                      )}
                </Paper>
            </Grid>
            <Grid item sm={6} >
                <Paper style={styles.Paper}>Right pane </Paper>

            </Grid>
        </Grid>
    }

}

export default Exercises;