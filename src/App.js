import React from 'react';
import './App.css';
import Header from './layouts/Header';
import Footer from './layouts/Footer';
import Exercises from './exercises/Exercises';
import { muscles } from './exercises/store'
import { statements } from '@babel/template';
import { tsConstructorType } from '@babel/types';

class App extends React.Component {
  
constructor(props)
{
  super(props)
  this.state={
     exercises:[
     {   id:1,
         title:'arms ',
         description:'doing arms',
         muscles:'arms'
     },
     {   id:2,
         title:'chest ',
         description:'doing chest',
         muscles:'chest'
     },
     {   id:3,
         title:'legs ',
         description:'doing legs',
         muscles:'legs'
     },
     {   id:4,
         title:'arms ',
         description:'doing back',
         muscles:'back'
     }
 ]
  }
}
  render() {
    return <div className="App">
      <Header>

      </Header>
      <br />
      <br />
      <Exercises exercises={this.state.exercises}>

      </Exercises>

      <Footer muscles={muscles}>

      </Footer>
    </div>
  }

  getExerciseByCategory() {
    return Object.entries(
      this.state.exercises.reduce((exercises, exercise) => {
        const { muscles } = exercise
        exercises[muscles] = exercises[muscles] ? [...exercises[muscles], exercise] : [exercise]
        return exercises
      }, {})
    )
  }

}


export default App;
