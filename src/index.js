import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import User from './user/User'
import Car from './car/Car'
import Header from './layouts/Header'
import * as serviceWorker from './serviceWorker';

const myElement=(
<table>
    <tr>
       <td> andreea</td>
       <td>diana</td>
    </tr>
    <tr>diana</tr>
</table>
 

)

ReactDOM.render(<App/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
