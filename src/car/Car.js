import React from 'react';
import User from '../user/User';
import './Car.css'

class Car extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            brand: "ford",
            model: 'mustang',
            color: "red",
            year: 1964,
            counter: 0,
            name:''
        }
    }
    changeName=(event)=>{
        let nam=event.target.name;
        let value=event.target.value;

       this.setState({
        [nam]:value
       })
    }
    mySubmitForm=(event)=>{
      

    }
    render() {
        const myprop = "my prop constant";
        return <div>
            <form onSubmit={this.mySubmitForm}>
                <input type='text' placeholder='name' name='brand' onChange={this.changeName}/>
                your name is {this.state.name}
                <button type='submit'>Submit form</button>
           

            hello from car <h3>car model: {this.state.model}</h3><h3>brand: {this.state.brand}  year:  {this.state.year} </h3> <User myProperty={myprop}></User>
            <button onClick={this.changeColor}>Change color </button>

            <textarea value={this.state.color+this.state.model}></textarea>
            <select value={this.state.brand} >
              <option value='andreea'>andreea</option>
              <option value='diana' style={{backgroundColor:"red"}}>diana</option>
              <option value='florica'>florica</option>
              <option value={this.state.brand}>{this.state.brand}</option>
            </select>
            </form>

        </div>
    }
    static getDerivedStateFromProps(props, state) {
        return { year: props.year }
    }
    changeColor = () => {

        this.setState({ brand: "new brand" + this.state.counter++ });
    }
}

export default Car;